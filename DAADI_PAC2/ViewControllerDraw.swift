//
//  ViewControllerDraw.swift
//  PR2S
//
//  Created by Javier Salvador Calvo on 14/10/16.
//  Copyright © 2016 UOC. All rights reserved.
//

import UIKit

class ViewControllerDraw: UIViewController {

       
    
    var m_str_json:String = ""
    var m_view:UIViewDraw? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // BEGIN-CODE-UOC-2
        // Create an instance of UIViewDraw with the view's size
        self.m_view = UIViewDraw(frame: self.view.frame)
        
        // Encode the received data in UTF-8
        let receivedData = m_str_json.data(using: String.Encoding.utf8)
        
        do {
            if let data = receivedData {
                
                // Parse the received data into a JSON object
                let jsonData = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)
                print("From the received data was the following JSON object parsed: \(jsonData)")
                
                if let jsonDictionary = (jsonData as? NSMutableDictionary) {
                    extractDataFromDictionary(dictionary: jsonDictionary)
                }
                
            } else {
                print("Error no data received")
            }
            
        } catch {
            print("Error deserializing JSON data: \(error)")
        }
        
        // Draw m_view according the received data adding it as a subview
        self.view.addSubview(m_view!)
    }

    func extractDataFromDictionary(dictionary: NSMutableDictionary) {
        // Loop over all dictionary keys and, when they match with the expected ones, load their values in the m_view variable
        for (key, value) in dictionary {
            switch (key as? String) {
            case "x-axis-labels":
                m_view?.m_x_axis_labels = (value as? NSMutableArray)
            case "y-axis":
                if let subValues = (value as? NSMutableDictionary) {
                    m_view?.m_y_axis_max_value = (subValues.value(forKey: "max-value") as! Double)
                    m_view?.m_y_axis_min_value = (subValues.value(forKey: "min-value") as! Double)
                    m_view?.m_y_axis_step = (subValues.value(forKey: "step") as! Double)
                }
            case "y-values":
                m_view?.m_y_values = (value as? NSMutableArray)
            default:
                break
            }
        }
        // END-CODE-UOC-2
    
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
