//
//  ViewControllerGallery.swift
//  PR2S
//
//  Created by Javier Salvador Calvo on 14/10/16.
//  Copyright © 2016 UOC. All rights reserved.
//

import UIKit

class ViewControllerGallery: UIViewController {
    
    @IBOutlet weak var  m_loader:UIActivityIndicatorView?
    
    var m_next_index:Int = 0
    var m_num_images:Int = 0
    var m_total_images:Int = 0
    
    var m_str_json:String = ""
    
    var m_images:NSMutableArray?=nil
    var m_views:NSMutableArray?=nil
    
    
    var m_timer:Timer?=nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // BEGIN-CODE-UOC-4
        // Animate the loader
        m_loader?.startAnimating()
        
        let receivedData = m_str_json.data(using: String.Encoding.utf8)
        
        do {
            if let data = receivedData {
                
                // Parse the received data into a JSON object
                let jsonData = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)
                print("From the received data was the following JSON object parsed: \(jsonData)")
                
                if let jsonArray = (jsonData as? NSMutableArray) {
                    // Initialize local variables
                    m_total_images = jsonArray.count
                    m_images = NSMutableArray(capacity: m_total_images)
                    m_views = NSMutableArray(capacity: m_total_images)
                    
                    // Loop over all positions in the array an execute OpenImage for each url, to load all image
                    for urlFromJson in jsonArray {
                        if let imageURL = (urlFromJson as? String) {
                            OpenImage(str_url: imageURL)
                        }
                    }
                }
            }
        } catch {
            print("Error deserializing JSON data: \(error)")
        }
        // END-CODE-UOC-4
        

    }
    
    
    @objc func AnimateRec(_ timer:Timer)
    {
        
        // BEGIN-CODE-UOC-7
        print("Timer to next animation ends")
        
        if let nextImageView = ((self.m_views?[self.m_next_index]) as? UIImageView) {
            
            let nextImageFrame: CGRect = nextImageView.frame
            
            // Move image outside the screen and bring it to front
            nextImageView.frame.origin.x = self.view.frame.maxX
            self.view.bringSubviewToFront(nextImageView)
            
            // Create and start UIImageView's animation
            print("Start animating the image of the index \(self.m_next_index)")
            UIView.animate(withDuration: 1.0, delay: 0.0, options: .curveEaseOut, animations: {
                
                // End position of the UIImageView
                nextImageView.frame = nextImageFrame
                
            }, completion: {finished in
                
                // Set the index of the next UIImageView to animate (restart if it reaches the zero)
                self.m_next_index = (self.m_next_index == 0) ? (self.m_total_images - 1) :  (self.m_next_index - 1)
                
                // Set a timer to execute AnimateRec function after 1 second to animate next UIImageView
                self.m_timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.AnimateRec), userInfo: nil, repeats: false)
                print("Timer to next animation starts")
                
            })
            
        } else {
            // Error while getting next UIImageView
            print("Error occurred getting next image")
            self.performSelector(onMainThread: #selector(self.AnimateRec), with: nil, waitUntilDone: false)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        
        // Stop timer when the ViewControllerGallery is not present
        m_timer?.invalidate()
        m_timer = nil
        // END-CODE-UOC-7
        
    }
    
    
    @objc func CreateViews()
    {
        // BEGIN-CODE-UOC-6
        if (self.m_images != nil) && (self.m_images?.count != 0) {
            
            // Create an UIImageView from each saved UImage
            for image in self.m_images! {
                if let newImage = (image as? UIImage) {
                    let imageView = UIImageView(frame: self.view.frame)
                    imageView.image = newImage
                    
                    // Scale the image to fill the screen and clip extra bounds
                    imageView.contentMode = .scaleAspectFill
                    imageView.clipsToBounds = true
                    
                    // Save UIImageView in m_views and add it as current view's subview
                    self.m_views?.add(imageView)
                    self.view.addSubview(imageView)
                }
            }
            
            // Set the index of the next UIView to show (currently displayed the last added). In the case of having only 3 images, it will be the position 1.
            self.m_next_index = self.m_num_images - 2
            
            // Stop animating the loader
            self.m_loader?.stopAnimating()
            
            // Set a timer to execute AnimateRec function after 1 second
            self.m_timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.AnimateRec), userInfo: nil, repeats: false)
            print("Timer to next animation starts")
        }
        // END-CODE-UOC-6
    
    }
    
 
    
    
    func OpenImage(str_url:String)
    {
        // BEGIN-CODE-UOC-5
        if let imageURL = URL(string: str_url) {
            let request: URLRequest = URLRequest(url: imageURL, cachePolicy: URLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 60)
            let session: URLSession = URLSession.shared
            
            // Create a task to load an image from its URL
            let task: URLSessionDataTask = session.dataTask(with: request) {
                data, response, error in
                
                if error != nil {
                    print("During the download from the URL \"\(imageURL)\" ocurred the error: \(error!)")
                    return
                } else {
                    print("Data loading from \"\(imageURL)\" ended succesfully")
                }
                
                // Create an UIImage from the received data and save it
                if let receivedImageData: Data = data {
                    if let newImage = UIImage(data: receivedImageData) {
                        self.m_images?.add(newImage)
                        self.m_num_images += 1
                        print("Added image number \(self.m_num_images) from \"\(imageURL)\"")
                    }
                }
                
                // When all the images has been downloaded start CreateViews function on main thread
                if self.m_total_images == self.m_num_images {
                    self.performSelector(onMainThread: #selector(self.CreateViews), with: nil, waitUntilDone: false)
                }
            }
            
            // Start loading task
            task.resume()
        }
        // END-CODE-UOC-5
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
